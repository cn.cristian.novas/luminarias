const enviar = document.querySelector(".enviar");
const selectorZona = document.getElementById("zona");
const selectorCalle = document.getElementById("calle");
const selectorNumeracion = document.getElementById("numeracion");
const selectorEntrecalle1 = document.getElementById("entrecalle1");
const selectorEntrecalle2 = document.getElementById("entrecalle2");
const selectorDefectuosas = document.getElementById("lumDefectuosas");
const selectorCorrectas = document.getElementById("lumCorrectas");

var zona;
var calle;
var numeracion;
var entrecalle1;
var entrecalle2;
var defectuosas;
var correctas;

selectorZona.addEventListener('change', function() {
    const opcionSeleccionada = this.options[selectorZona.selectedIndex];
    zona = opcionSeleccionada.text;
});

selectorCalle.addEventListener('change', function() {
    calle = this.value;
});

selectorNumeracion.addEventListener('change', function() {
    numeracion = this.value;
});

selectorEntrecalle1.addEventListener('change', function() {
    entrecalle1 = this.value;
});

selectorEntrecalle2.addEventListener('change', function() {
    entrecalle2 = this.value;
});

selectorDefectuosas.addEventListener('change', function() {
    defectuosas = this.value;
});

selectorCorrectas.addEventListener('change', function() {
    correctas = this.value;
});


enviar.addEventListener("click", (e) => {
    e.preventDefault();

    data = {
        zona,
        calle,
        numeracion,
        entrecalle1,
        entrecalle2,
        defectuosas,
        correctas
    }

    fetch(`/api/luminarias`, {
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST"
        })
        .then((response) => response.json())
        .then((data) => {
            alert(data.mensaje);
            document.getElementById("formLuminarias").reset();
        }).catch(() => {
            alert("Ocurrio un error al guardar los datos");
        });

});