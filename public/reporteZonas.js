var contenido = document.getElementById("contenido");;

function getData() {


    fetch(`/api/luminarias/reporte/zonas`, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
        .then((response) => response.json())
        .then((datos) => {
            dibujarTabla(datos);
        }).catch(() => {
            alert("Ocurrio un error el mostrar los datos");
        });
}

function dibujarTabla(datos) {

    contenido.innerHTML = '';

    for (var key in datos) {
        var value = datos[key];
        console.log(value);

        contenido.innerHTML += `
            <p>Zona ${key} --- Total Defectuosas ${value.totalDefectuosas} --- Total Correctas ${value.totalCorrectas}</p>
        `

        for (let registro of value.cuadras) {
            contenido.innerHTML += `
            <tr >
                <th scope="row"></th>
                <td >${registro.calle  ? registro.calle : "-" }</td>
                <td>${registro.numeracion ? registro.numeracion : "-" }</td>
                <td>${registro.entrecalle1  ? registro.entrecalle1 : "-" }</td>
                <td>${registro.entrecalle2  ? registro.entrecalle2 : "-" }</td>
                <td>${registro.defectuosas  ? registro.defectuosas : "-" }</td>
                <td>${registro.correctas  ? registro.correctas : "-" }</td>
            </tr>
            `
        }
    }

}

getData();