var contenido = document.getElementById("contenido");;

function getData() {


    fetch(`/api/luminarias`, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "GET"
        })
        .then((response) => response.json())
        .then((datos) => {
            dibujarTabla(datos);
        }).catch(() => {
            alert("Ocurrio un error al guardar los datos");
        });
}

function dibujarTabla(datos) {

    contenido.innerHTML = '';
    for (let registro of datos) {
        contenido.innerHTML += `
        <tr >
            <th scope="row">${registro.zona  ? registro.zona : "-" }</th>
            <td >${registro.calle  ? registro.calle : "-" }</td>
            <td>${registro.numeracion ? registro.numeracion : "-" }</td>
            <td>${registro.entrecalle1  ? registro.entrecalle1 : "-" }</td>
            <td>${registro.entrecalle2  ? registro.entrecalle2 : "-" }</td>
            <td>${registro.defectuosas  ? registro.defectuosas : "-" }</td>
            <td>${registro.correctas  ? registro.correctas : "-" }</td>
        </tr>
        `
    }
}

getData();