const enviar = document.querySelector(".enviar");
const selectorNombre = document.getElementById("nombre");
const selectorZona = document.getElementById("zona");
const selectorFecha = document.getElementById("fecha");

var nombre;
var zona;
var fecha;

selectorNombre.addEventListener('change', function() {
    const opcionSeleccionada = this.options[selectorNombre.selectedIndex];
    nombre = opcionSeleccionada.text;
});

selectorZona.addEventListener('change', function() {
    const opcionSeleccionada = this.options[selectorZona.selectedIndex];
    zona = opcionSeleccionada.text;
});

selectorFecha.addEventListener('change', function() {
    const dateControl = document.querySelector('input[type="date"]');
    fecha = dateControl.value;
});

enviar.addEventListener("click", (e) => {
    e.preventDefault();

    data = {
        "inspector": nombre,
        "zona": zona,
        "fecha": fecha
    }

    fetch(`/api/inspectores`, {
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST"
        })
        .then((response) => response.json())
        .then((mensaje) => {
            alert(mensaje.mensaje + " - Nombre: " + nombre + " - Zona: " + zona + " - Fecha: " + fecha);
            document.getElementById("formInpectores").reset();
        }).catch(() => {
            alert("Ocurrio un error al guardar los datos");
        });





});