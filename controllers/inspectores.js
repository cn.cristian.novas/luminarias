const { response } = require('express');
const { leerDB, guardarDB } = require('./utils');

const archivo = './db/inspectores.json';

const asignarInspector = (req, res = response) => {
    const body = req.body;
    var listado = leerDB(archivo);
    listado.push(body);
    guardarDB(archivo, listado);

    res.json({ "mensaje": "Asignación guardada correctamente" });
}

const listaInspectores = (req, res = response) => {
    var listado = leerDB(archivo);
    res.json(listado);
}

const setDatos = (req, res = response) => {
    var listado = leerDB(archivo);
    listado.length = 0;
    listado.push({
        "inspector": "Gomez, Martin",
        "zona": "A1",
        "fecha": "2021-12-01"
    }, {
        "inspector": "Valenzuela, Roberto",
        "zona": "A2",
        "fecha": "2021-12-02"
    });
    guardarDB(archivo, listado);
    res.json({ "mensaje": "Archivo seteado correctamente" });
}

module.exports = {
    asignarInspector,
    listaInspectores,
    setDatos
}