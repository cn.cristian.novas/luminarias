const { response } = require('express');
const { leerDB, guardarDB } = require('./utils');

const archivo = './db/luminarias.json';

const crearRegistro = (req, res = response) => {
    const body = req.body;
    var listado = leerDB(archivo);
    listado.push(body);
    guardarDB(archivo, listado);

    res.json({ "mensaje": "Registro guardado correctamente" });
}

const actualizarRegistro = (req, res = response) => {

    const id = req.params.id;
    res.json('actualizarRegistro ' + id);
}

const listarLuminarias = (req, res = response) => {

    var listado = leerDB(archivo);
    res.json(listado);
}

const reporteCritico = (req, res = response) => {

    var listado = leerDB(archivo);
    let criticas = [];

    if (listado.length > 0) {
        criticas.push(listado[0]);

        for (let index = 1; index < listado.length; index++) {
            const cuadra = listado[index];

            if (parseInt(cuadra.defectuosas) == parseInt(criticas[0].defectuosas)) {
                criticas.push(cuadra);

            } else if (parseInt(cuadra.defectuosas) > parseInt(criticas[0].defectuosas)) {
                criticas.length = 0;
                criticas.push(cuadra);
            }
        }
    }

    res.json(criticas);
}

const reporteZonas = (req, res = response) => {

    var listado = leerDB(archivo);
    var zonas = {};

    listado.forEach(cuadra => {
        console.log("Estado de array zonas: " + zonas[cuadra.zona]);
        if (zonas[cuadra.zona]) {
            zonas[cuadra.zona]["cuadras"].push(cuadra);
            zonas[cuadra.zona]["totalDefectuosas"] += parseInt(cuadra.defectuosas);
            zonas[cuadra.zona]["totalCorrectas"] += parseInt(cuadra.correctas);

        } else {
            zonas[cuadra.zona] = {};
            zonas[cuadra.zona]["cuadras"] = [];
            zonas[cuadra.zona]["cuadras"].push(cuadra);
            zonas[cuadra.zona]["totalDefectuosas"] = parseInt(cuadra.defectuosas);
            zonas[cuadra.zona]["totalCorrectas"] = parseInt(cuadra.correctas);
        }
    });

    res.json(zonas);
}

const setDatos = (req, res = response) => {
    var listado = leerDB(archivo);
    console.log(listado);
    listado.length = 0;
    listado.push({
        "zona": "A1",
        "calle": "Casa1",
        "numeracion": "1666",
        "entrecalle1": "186",
        "entrecalle2": "187",
        "defectuosas": "2",
        "correctas": "5"
    }, {
        "zona": "A1",
        "calle": "Casa2",
        "numeracion": "222",
        "entrecalle1": "111",
        "entrecalle2": "333",
        "defectuosas": "0",
        "correctas": "6"
    });
    guardarDB(archivo, listado);
    res.json({ "mensaje": "Archivo seteado correctamente" });
}

module.exports = {
    crearRegistro,
    actualizarRegistro,
    listarLuminarias,
    reporteCritico,
    reporteZonas,
    setDatos
}