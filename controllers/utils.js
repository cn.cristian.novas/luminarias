const fs = require('fs');

const guardarDB = (archivo, data) => {
    fs.writeFileSync(archivo, JSON.stringify(data));
}

const leerDB = (archivo) => {

    if (!fs.existsSync(archivo)) {
        return null;
    }

    const info = fs.readFileSync(archivo, { encoding: 'utf-8' });
    const data = JSON.parse(info);

    return data;
}

module.exports = {
    guardarDB,
    leerDB
}