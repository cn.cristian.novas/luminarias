const express = require('express');
const cors = require('cors');

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.luminariasPath = '/api/luminarias';
        this.inspectoresPath = '/api/inspectores';

        //path para zonas

        this.middelware();

        this.routes();

    }

    middelware() {
        //cors
        this.app.use(cors());

        //body format
        this.app.use(express.json());

        //public
        this.app.use(express.static('public'));
    }


    routes() {

        this.app.use(this.luminariasPath, require('../routes/luminarias'));
        this.app.use(this.inspectoresPath, require('../routes/inspectores'));
    }


    listen() {
        this.app.listen(this.port, () => {
            console.log(`App escuchando en http://localhost:${this.port}`);
        })
    }

}

module.exports = Server;