const { Router } = require('express');
const {
    asignarInspector,
    listaInspectores,
    setDatos
} = require('../controllers/inspectores');
const router = Router();


router.post('/', asignarInspector)

router.get('/', listaInspectores)

router.post('/setDatos', setDatos)

module.exports = router;