const { Router } = require('express');
const {
    crearRegistro,
    actualizarRegistro,
    listarLuminarias,
    reporteCritico,
    reporteZonas,
    setDatos
} = require('../controllers/luminarias');
const router = Router();


router.post('/', crearRegistro)

router.put('/:id', actualizarRegistro)

router.get('/', listarLuminarias)

router.get('/reporte/criticas', reporteCritico)

router.get('/reporte/zonas', reporteZonas)

router.post('/setDatos', setDatos)

/*
router.get('/asignarInspector', (req, res) => {
    res.sendFile(__dirname + '/public/asignarInspector.html')
})

router.get('/reportes', (req, res) => {
    res.sendFile(__dirname + '/public/reportes.html')
})
 */

module.exports = router;